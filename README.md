# Rock paper scissors development task
by Iván Francisco Pérez Reina

## Run backend server local test
java -jar rock_paper_scissors-0.0.1-SNAPSHOT.jar

## Run angular server local test
npm install http-server -g

http-server -p 4200 dist/rockpaperscissors

or follow rockpaperscissors/README.md to run from source code

Test at http://localhost:4200/

## Soruce code
Spring Boot App: rockpaperscissors-server\src

Agular app: rockpaperscissors\src