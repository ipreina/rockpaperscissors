package com.techtask.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techtask.model.GameRound;
import com.techtask.model.Statistics;
import com.techtask.service.GameService;

/**
 * Rock paper scissors Game REST Controller
 */
@RestController
@CrossOrigin
public class GameController {

  @Autowired
  private GameService gameService;

  /** Logger */
  private static final Logger LOGGER = LoggerFactory.getLogger(GameController.class);

  /**
   * Play a random round
   * @return
   */
  @GetMapping(value = "/play", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<GameRound> playRound() {
    LOGGER.info("Play new round");
    try {
      return new ResponseEntity<>(gameService.playRound(), HttpStatus.OK);
    } catch (Exception e) {
      LOGGER.error("Error playing new round", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Gets statistics for played game rounds
   * @return
   */
  @GetMapping(value = "/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Statistics> getStatistics() {
    LOGGER.info("Calculate statistics");
    try {
      return new ResponseEntity<>(gameService.calculateRoundsStatistics(), HttpStatus.OK);
    } catch (Exception e) {
      LOGGER.error("Error calculating statistics", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
