package com.techtask.model;

/**
 * Rock paper scissors game options
 */
public enum GameOption {
    ROCK,
    PAPER,
    SCISSORS;
  }