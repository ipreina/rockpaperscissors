package com.techtask.model;

/**
 * Rock paper scissors game result
 */
public enum GameResult {
    PLAYER1_WINS,
    PLAYER2_WINS,
    DRAW
  }