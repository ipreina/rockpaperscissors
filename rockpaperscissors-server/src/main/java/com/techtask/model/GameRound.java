package com.techtask.model;

/**
 * Rock paper scissors game round
 */
public class GameRound {

  /** Option choosen by player 1 */
  private GameOption player1Option;

  /** Option choosen by player 2 */
  private GameOption player2Option;

  /** Round result */
  private GameResult result;

  /**
   * default constructor
   */
  public GameRound() {
    super();
  }

  /**
   * Constructor
   * @param player1Option Option choosen by player 1
   * @param player2Option Option choosen by player 2
   * @param result Round result
   */
  public GameRound(GameOption player1Option, GameOption player2Option, GameResult result) {
    this.player1Option = player1Option;
    this.player2Option = player2Option;
    this.result = result;
  }

  /**
   * @return the player1Option
   */
  public GameOption getPlayer1Option() {
    return player1Option;
  }

  /**
   * @return the player2Option
   */
  public GameOption getPlayer2Option() {
    return player2Option;
  }

  /**
   * @return the result
   */
  public GameResult getResult() {
    return result;
  }

}