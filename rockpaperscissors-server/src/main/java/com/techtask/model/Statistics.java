package com.techtask.model;

/**
 * Played game rounds statictis
 */
public class Statistics {

  /** Total rounds played */
  private int totalRounds;

  /** Total Wins for first players */
  private int totalPlayer1Wins;

  /** Total Wins for second players */
  private int totalPlayer2Wins;

  /** Total draws */
  private int totalDraws;

  /**
   * @return the toltaRounds
   */
  public int getTotalRounds() {
    return totalRounds;
  }

  /**
   * @param totalRounds the toltaRounds to set
   */
  public void setTotalRounds(int totalRounds) {
    this.totalRounds = totalRounds;
  }

  /**
   * @return the totalPlayer1Wins
   */
  public int getTotalPlayer1Wins() {
    return totalPlayer1Wins;
  }

  /**
   * @param totalPlayer1Wins the totalPlayer1Wins to set
   */
  public void setTotalPlayer1Wins(int totalPlayer1Wins) {
    this.totalPlayer1Wins = totalPlayer1Wins;
  }

  /**
   * @return the totalPlayer2Wins
   */
  public int getTotalPlayer2Wins() {
    return totalPlayer2Wins;
  }

  /**
   * @param totalPlayer2Wins the totalPlayer2Wins to set
   */
  public void setTotalPlayer2Wins(int totalPlayer2Wins) {
    this.totalPlayer2Wins = totalPlayer2Wins;
  }

  /**
   * @return the totalDraws
   */
  public int getTotalDraws() {
    return totalDraws;
  }

  /**
   * @param totalDraws the totalDraws to set
   */
  public void setTotalDraws(int totalDraws) {
    this.totalDraws = totalDraws;
  }

}
