package com.techtask.service;

import com.techtask.model.GameOption;
import com.techtask.model.GameResult;

/**
 * Rock paper scissors game evaluator
 */
public class GameEvaluator {

  private static GameOption winnerOption(GameOption op1) {
    switch (op1) {
    case PAPER:
      return GameOption.SCISSORS;
    case ROCK:
      return GameOption.PAPER;
    case SCISSORS:
      return GameOption.ROCK;
    default:
      return null;
    }
  }

  /**
   * Determines the result of a game round
   * 
   * @param player1Option Option choosen by player 1
   * @param player2Option Option choosen by player 2
   * @return result of the round
   */
  public static GameResult evaluateResult(GameOption player1Option, GameOption player2Option) {
    if (player1Option.equals(player2Option)) {
      return GameResult.DRAW;
    }
    return player1Option.equals(winnerOption(player2Option)) ? GameResult.PLAYER1_WINS : GameResult.PLAYER2_WINS;
  }

}
