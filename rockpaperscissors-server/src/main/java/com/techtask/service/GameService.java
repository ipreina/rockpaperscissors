package com.techtask.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Component;

import com.techtask.model.GameOption;
import com.techtask.model.GameResult;
import com.techtask.model.GameRound;
import com.techtask.model.Statistics;

/**
 * Rock paper scissors game service
 */
@Component
public class GameService {

  /** Player 1 option chooser */
  private IPlayer player1;

  /** Player 2 option chooser */
  private IPlayer player2;

  /** Played round list */
  private List<GameRound> playedRoundList = Collections.synchronizedList(new ArrayList<>());

  /** Posible player options */
  private GameOption[] gameOptionList = GameOption.values();

  /**
   * Constructor
   */
  public GameService() {
    player1 = this::chooseRandomOption;
    player2 = () -> GameOption.ROCK;
  }

  /**
   * Choose randomly a game option
   * @return game option
   */
  private GameOption chooseRandomOption() {
    return gameOptionList[ThreadLocalRandom.current().nextInt(gameOptionList.length)];
  }

  /**
   * Play new Round
   * @return Game result
   */
  public GameRound playRound() {
    GameOption player1Option = player1.chooseOption();
    GameOption player2Option = player2.chooseOption();
    GameResult result = GameEvaluator.evaluateResult(player1Option, player2Option);
    GameRound round = new GameRound(player1Option, player2Option, result);
    playedRoundList.add(round);
    return round;
  }

  /**
   * Totalize round results
   * @return Round result statistics
   */
  public Statistics calculateRoundsStatistics() {
    StatisticsCalculator statisticsCalculator = new StatisticsCalculator();
    getPlayedRoundList().forEach(statisticsCalculator::processRound);
    return statisticsCalculator.getStatistics();
  }

  /**
   * @return the playedRoundList
   */
  public List<GameRound> getPlayedRoundList() {
    return Collections.unmodifiableList(playedRoundList);
  }


}
