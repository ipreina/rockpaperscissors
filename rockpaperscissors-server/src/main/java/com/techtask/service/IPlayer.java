package com.techtask.service;

import com.techtask.model.GameOption;

/**
 * Player option chooser
 */
@FunctionalInterface
public interface IPlayer {

  /**
   * Choose a game option
   */
  public GameOption chooseOption();

}
