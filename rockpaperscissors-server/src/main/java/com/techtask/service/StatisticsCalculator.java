package com.techtask.service;

import com.techtask.model.GameRound;
import com.techtask.model.Statistics;

/**
 * Calculates round statistics
 */
public class StatisticsCalculator {

  /** Statistics data */
  private Statistics statistics;

  /**
   * Constructor
   */
  public StatisticsCalculator() {
    statistics = new Statistics();
  }

  /**
   * Add round data to statistics
   * @param round
   */
  public void processRound(GameRound round) {
    switch (round.getResult()) {
    case PLAYER1_WINS:
      getStatistics().setTotalPlayer1Wins(getStatistics().getTotalPlayer1Wins() + 1);
      break;
    case PLAYER2_WINS:
      getStatistics().setTotalPlayer2Wins(getStatistics().getTotalPlayer2Wins() + 1);
      break;
    case DRAW:
      getStatistics().setTotalDraws(getStatistics().getTotalDraws() + 1);
      break;
    }
    getStatistics().setTotalRounds(getStatistics().getTotalRounds() + 1);
  }

  /**
   * @return the statistics
   */
  public Statistics getStatistics() {
    return statistics;
  }

}
