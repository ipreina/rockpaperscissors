package com.techtask;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techtask.model.GameOption;
import com.techtask.model.GameResult;
import com.techtask.model.GameRound;
import com.techtask.model.Statistics;
import com.techtask.service.GameService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTests {

  @MockBean
  private GameService gameService;

  /**
   * Mapper json
   */
  private ObjectMapper mapper = new ObjectMapper();

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void testPlay() throws Exception {
    GameRound round = new GameRound(GameOption.PAPER, GameOption.ROCK, GameResult.PLAYER1_WINS);
    when(gameService.playRound()).thenReturn(round);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/play").accept(MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    GameRound resultRound = mapper.readValue(result.getResponse().getContentAsString(), GameRound.class);

    assertEquals(round.getPlayer1Option(), resultRound.getPlayer1Option());
    assertEquals(round.getPlayer2Option(), resultRound.getPlayer2Option());
    assertEquals(round.getResult(), resultRound.getResult());
  }

  @Test
  public void testStatistics() throws Exception {
    Statistics statistics = new Statistics();
    statistics.setTotalRounds(10);
    statistics.setTotalPlayer1Wins(2);
    statistics.setTotalPlayer2Wins(3);
    statistics.setTotalDraws(5);
    when(gameService.calculateRoundsStatistics()).thenReturn(statistics);
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/statistics").accept(MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    Statistics resultStatistics = mapper.readValue(result.getResponse().getContentAsString(), Statistics.class);

    assertEquals(statistics.getTotalDraws(), resultStatistics.getTotalDraws());
    assertEquals(statistics.getTotalPlayer1Wins(), resultStatistics.getTotalPlayer1Wins());
    assertEquals(statistics.getTotalPlayer2Wins(), resultStatistics.getTotalPlayer2Wins());
    assertEquals(statistics.getTotalRounds(), resultStatistics.getTotalRounds());
  }

  @Test
  public void testPlayError() throws Exception {
    when(gameService.playRound()).thenThrow(new NullPointerException());
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/play").accept(MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getResponse().getStatus());
  }

  @Test
  public void testStatisticsError() throws Exception {
    when(gameService.calculateRoundsStatistics()).thenThrow(new NullPointerException());
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/statistics").accept(MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getResponse().getStatus());
  }

}
