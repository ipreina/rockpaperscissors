package com.techtask;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.techtask.model.GameOption;
import com.techtask.model.GameResult;
import com.techtask.service.GameEvaluator;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class GameEvaluatorTest {

  @Test
  public void testDraw() throws Exception {
    GameResult result = GameEvaluator.evaluateResult(GameOption.PAPER, GameOption.PAPER);
    assertEquals(GameResult.DRAW, result);
    result = GameEvaluator.evaluateResult(GameOption.ROCK, GameOption.ROCK);
    assertEquals(GameResult.DRAW, result);
    result = GameEvaluator.evaluateResult(GameOption.SCISSORS, GameOption.SCISSORS);
    assertEquals(GameResult.DRAW, result);
  }

  @Test
  public void testPlayer1Win() throws Exception {
    GameResult result = GameEvaluator.evaluateResult(GameOption.PAPER, GameOption.ROCK);
    assertEquals(GameResult.PLAYER1_WINS, result);
    result = GameEvaluator.evaluateResult(GameOption.ROCK, GameOption.SCISSORS);
    assertEquals(GameResult.PLAYER1_WINS, result);
    result = GameEvaluator.evaluateResult(GameOption.SCISSORS, GameOption.PAPER);
    assertEquals(GameResult.PLAYER1_WINS, result);
  }

  @Test
  public void testPlayer2Win() throws Exception {
    GameResult result = GameEvaluator.evaluateResult(GameOption.PAPER, GameOption.SCISSORS);
    assertEquals(GameResult.PLAYER2_WINS, result);
    result = GameEvaluator.evaluateResult(GameOption.ROCK, GameOption.PAPER);
    assertEquals(GameResult.PLAYER2_WINS, result);
    result = GameEvaluator.evaluateResult(GameOption.SCISSORS, GameOption.ROCK);
    assertEquals(GameResult.PLAYER2_WINS, result);
  }

}
