package com.techtask;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.techtask.model.GameOption;
import com.techtask.model.GameRound;
import com.techtask.model.Statistics;
import com.techtask.service.GameService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class GameServiceTest {

  @Autowired
  private GameService gameService;

  @Test
  public void testPlay() throws Exception {
    GameRound round = gameService.playRound();
    assertNotNull(round);
    assertNotNull(round.getPlayer1Option());
    assertEquals(GameOption.ROCK, round.getPlayer2Option());
    assertNotNull(round.getResult());
  }

  @Test
  public void testPlayedRoundList() throws Exception {
    List<GameRound> playedRoundList = gameService.getPlayedRoundList();
    int initSize = playedRoundList.size();
    GameRound round = gameService.playRound();
    playedRoundList = gameService.getPlayedRoundList();
    assertEquals(initSize + 1, playedRoundList.size());
    GameRound lastRound = playedRoundList.get(playedRoundList.size() - 1);

    assertEquals(round.getPlayer1Option(), lastRound.getPlayer1Option());
    assertEquals(round.getPlayer2Option(), lastRound.getPlayer2Option());
    assertEquals(round.getResult(), lastRound.getResult());
  }

  @Test
  public void testStatistics() throws Exception {
    gameService.playRound();
    Statistics stats = gameService.calculateRoundsStatistics();

    assertTrue(stats.getTotalRounds() > 0);
    assertTrue((stats.getTotalPlayer1Wins() + stats.getTotalPlayer2Wins() + stats.getTotalDraws()) > 0);
  }

}
