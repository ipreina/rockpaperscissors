package com.techtask;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.techtask.model.GameOption;
import com.techtask.model.GameResult;
import com.techtask.model.GameRound;
import com.techtask.model.Statistics;
import com.techtask.service.StatisticsCalculator;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class StatisticsEvaluatorTest {

  @Test
  public void testInitialValue() throws Exception {
    StatisticsCalculator calculator = new StatisticsCalculator();
    Statistics statistics = calculator.getStatistics();
    assertNotNull(statistics);
    assertEquals(0, statistics.getTotalDraws());
    assertEquals(0, statistics.getTotalPlayer1Wins());
    assertEquals(0, statistics.getTotalPlayer2Wins());
    assertEquals(0, statistics.getTotalRounds());
  }

  @Test
  public void testStatistics() throws Exception {
    StatisticsCalculator calculator = new StatisticsCalculator();
    calculator.processRound(new GameRound(GameOption.PAPER, GameOption.PAPER, GameResult.DRAW));
    calculator.processRound(new GameRound(GameOption.ROCK, GameOption.ROCK, GameResult.DRAW));
    calculator.processRound(new GameRound(GameOption.PAPER, GameOption.ROCK, GameResult.PLAYER1_WINS));
    calculator.processRound(new GameRound(GameOption.ROCK, GameOption.SCISSORS, GameResult.PLAYER1_WINS));
    calculator.processRound(new GameRound(GameOption.PAPER, GameOption.SCISSORS, GameResult.PLAYER2_WINS));
    Statistics statistics = calculator.getStatistics();
    assertNotNull(statistics);
    assertEquals(2, statistics.getTotalDraws());
    assertEquals(2, statistics.getTotalPlayer1Wins());
    assertEquals(1, statistics.getTotalPlayer2Wins());
    assertEquals(5, statistics.getTotalRounds());
  }

}
