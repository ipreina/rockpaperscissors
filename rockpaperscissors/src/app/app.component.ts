import { Component } from '@angular/core';
import { GameRound } from '../models/GameRound';
import { Statistics } from '../models/Statistics';
import { RestService } from './services/rest.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private service: RestService) { }
  
  play() {
    this.service.play().subscribe(round => {this.list.push(round); this.rounds++});
  }

  reset() {
    this.list = [];
    this.rounds = 0;
  }

  showStatisticsInfo(){
    this.service.statistics().subscribe(result => (this.stats=result));
    this.hideStatistics = false;
  }

  hideStatisticsInfo(){
    this.hideStatistics = true;
  }

  title = 'rockpaperscissors';
  rounds: number = 0;
  list: GameRound[] = [];
  hideStatistics = true;
  stats: Statistics = {
    totalRounds: 0,
    totalPlayer1Wins: 0,
    totalPlayer2Wins: 0,    
    totalDraws: 0
  };

}
