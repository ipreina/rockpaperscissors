import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GameRound } from '../../models/GameRound';
import { Statistics } from '../../models/Statistics';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  private url = "http://localhost:8080/play"; 
  private urlStat = "http://localhost:8080/statistics"; 

  public play(): Observable<GameRound> {
    return this.http.get<GameRound>(this.url);
  }

  public statistics(): Observable<Statistics> {
    return this.http.get<Statistics>(this.urlStat);
  }

}
