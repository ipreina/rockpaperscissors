export class Statistics{
    totalRounds: number;
    totalPlayer1Wins: number;
    totalPlayer2Wins: number;    
    totalDraws: number;    
}